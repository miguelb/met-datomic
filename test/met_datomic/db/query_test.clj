(ns met-datomic.db.query-test
  (:require [clojure.test :refer :all]
            [met-datomic.db.query :refer :all]
            [met-datomic.db.manage :as manage]
            [met-datomic.config :refer [config]]
            [datomic.api :as d]))


(def test-db-uri (config :datomic :test-uri))

(defn setup-test-db [f]
  (manage/init-db test-db-uri)
  (f)
  (manage/delete test-db-uri))

(use-fixtures :each setup-test-db)

(deftest verify-database
  (let [total-artist 3758
        total-works 11351]
    (testing "Assumptions after seed data"
      (is (= total-artist (count (artist-entities))))
      (is (= total-works (count (work-entities)))))))


(deftest search-for-stuff
  (let [entityid 17592186059416
        artist-name "Pablo Picasso"]
    (testing "Find by Artist Hash"
      (is (= entityid (find-artist-by-hash "9716719fb465c4004381d597ac4a7107"))))
    (testing "Find by Entity ID"
      (is (= entityid (find-by-id 17592186059416))))
    (testing "Find Works by Artist's name, strict name"
      (is (= 36 (count (find-by-artist-name artist-name)))))
    (testing "Find All Works for All Artists by Artist"
      (is (= 2 (count (all-works-by-artist 2))))
      (is (= "Oak Tree" (-> (all-works-by-artist 2)
                                         flatten
                                         second
                                         :work/title
                                         ))))))


