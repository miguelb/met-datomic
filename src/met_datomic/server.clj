(ns met-datomic.server
  (:require [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [met-datomic.routes.site :as routes])
  (:use [org.httpkit.server :only [run-server]]))

(defonce server (atom nil))

(defn stop-server
  "Graceful shutdown: wait 100ms for existing request to be finished"
  []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(defn -main
  "docstring"
  [& [port]]
  (let [port (if port (Integer/parseInt port) 3000)]
    (reset! server (run-server #'routes/handler {:port port}))
    (println (format "App running at port %d..." port))))


