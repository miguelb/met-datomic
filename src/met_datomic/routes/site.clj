(ns met-datomic.routes.site
  (:import (java.io ByteArrayOutputStream))
  (:require [bidi.bidi :as bidi]
            [bidi.ring :refer (make-handler)]
            [liberator.core :refer (defresource)]
            [liberator.dev :refer (wrap-trace)]
            [met-datomic.db.query :as q]
            [cognitect.transit :as transit]
            [clojure.java.io :as io]
            [ring.util.response :refer :all]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.params :refer [params-request]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.util.mime-type :refer [ext-mime-type]]
            ))

(defn write [x]
  (let [baos (ByteArrayOutputStream.)
        w    (transit/writer baos :json)
        -    (transit/write w x)
        ret  (.toString baos)]
    (.reset baos)
    ret))

(defn writev [x]
  (let [baos (ByteArrayOutputStream.)
        wv (transit/writer baos :json-verbose)
        - (transit/write wv x)
        ret (.toString baos)]
    (.reset baos)
    ret))

(defn last-modified [f]
  (cond
    (not (.exists f)) 0
    (.isFile f) (.lastModified f)
    (.isDirectory f) (reduce max (map last-modified (.listFiles f)))))

(let [static-dir (io/file "resources/public")]
  (defresource static

    :available-media-types
    (fn [ctx]
      (let [path (or (get-in ctx [:request :route-params :*]) "index.html")]
        (if-let [mime-type (ext-mime-type path)]
          [mime-type]
          [])))

    :exists?
    (fn [ctx]
      (let [path (or (get-in ctx [:request :route-params :*]) "index.html")]
        (let [f (io/file static-dir path)]
          [(.exists f) {::file f}])))

    :handle-ok (fn [ctx]
                 (get-in ctx [::file]))

     ;(fn [{f ::file}] (.lastModified f))
    ))

(def routes
  ["" {"/" :index
       "/artists" :artists
       "/worksbyartist" :works-by-artist
       }])

(defn list-works-fn [ctx]
  (let [req-w-params (params-request (get-in ctx [:request]))
        limit (read-string (or (get-in req-w-params [:params "limit"]) "0"))]
    (if (zero? limit)
      (q/all-works-by-artist)
      (q/all-works-by-artist limit))))


(defn super-flatten [body]
  (->>
    body
    (into {})))

(defresource list-works-by-artist-resource
  :available-media-types ["application/json"
                          "application/transit+json"]
  :allowed-methods [:get]
  :handle-ok (fn [ctx]
               (let [media-type (get-in ctx [:representation :media-type])
                     body (list-works-fn ctx)]
                 (condp = media-type
                   "application/json" (writev (super-flatten body))
                   "application/transit+json" (write (super-flatten body)))
                 )))

(defresource list-artists-resource
  :available-media-types ["application/json"]
  :allowed-methods [:get]
  :handle-ok (fn [_] (q/artist-entities)))


(def handler
  (->
    (make-handler routes {:index static
                          :artists list-artists-resource
                          :works-by-artist list-works-by-artist-resource})
    (wrap-trace :header :ui)
    (wrap-cors :access-control-allow-origin [#".*"]
      :access-control-allow-methods [:get :put :post :delete])))