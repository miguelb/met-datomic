(ns met-datomic.db.query
  (:require [datomic.api :as d :refer [q db]])
  (:use met-datomic.config))


(def ^:dynamic *db-uri* (config :datomic :db-uri))

(def conn
  (d/connect *db-uri*))

(defn find-artist-by-hash
  "Find an artist by using the hashed version of its name"
  [name-hash]
  (let [query '[:find ?id .
                :in $ ?hash
                :where
                [?id :artist/hash ?hash]]]
    (q query (db conn) name-hash)))

(defn find-by-id
  "Find an entity by its Entity ID"
  [eid]
  (let [query '[:find ?eid .
                :in $ ?eid
                :where
                [?eid]]]
    (q query (db conn) eid)))

(defn find-by-artist-name
  "Find a work by the artist name (strict)"
  [name]
  (let [query '[:find ?work ?title ?metid
                :in $ ?artist-name
                :where
                [?artist :artist/name ?artist-name]
                [?work :work/artist ?artist]
                [?work :work/title ?title]
                [?work :work/metid ?metid]]]
    (q query (db conn) name)))

(defn artist-entities
  "Find all artists in db"
  []
  (let [query '[:find [?artist ...]
                :where
                [?artist :artist/name]]]
    (q query (db conn))))

(defn work-entities
  "Find all works in db"
  []
  (let [query '[:find [?work ...]
                :where
                [?work :work/metid]]]
    (q query (db conn))))

(defn work-entity->map [work]
  (-> (into {} work)
      (dissoc :work/artist)))

(defn process-work-entities
  "Convert all nested structures to Maps"
  [coll]
  (->> (map d/touch coll)
       (map work-entity->map)
       flatten))

(defn all-works-by-artist [& mlimit]
  (let [artists (artist-entities)
        entity-collection (if (nil? mlimit) artists (take (first mlimit) artists))]
    (map (fn [artist-entity]
           (let [entity (d/entity (db conn) artist-entity)
                 entity-works (process-work-entities (:work/_artist entity))]
             [(:artist/name entity) entity-works])) entity-collection)))
