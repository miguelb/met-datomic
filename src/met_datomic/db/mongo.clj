(ns met-datomic.db.mongo
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [monger.query :refer :all]
            [datomic.api :as d]
            [met-datomic.db.manage :as datomic-mgr]
            [met-datomic.db.query :as datomic-q])
  (:import [com.mongodb MongoOptions ServerAddress]
           (java.security MessageDigest)
           (java.math BigInteger))
  (:use clojure.pprint))

(use 'environ.core)

;; set REPL max print length
;(set! *print-length* 250)

;; set DATOMIC Timeout to 30sec
(System/setProperty "datomic.txTimeoutMsec" "30000")
;;

;; relevant fields for each work in Mongo
(def relevant-fields #{:id :title :who :when (keyword "accession number") :_links :description})
(def source-key :work/url)
(def related-works-key :related-artwork-ids)


(def datomic-mongo-key-map
  {:title                       :work/title
   :when                        :work/date
   (keyword "accession number") :work/acc-num
   :source                      :work/url
   :description                 :work/desc
   :who                         :artist/name
   :id                          :work/metid})

(def not-nil? (complement nil?))

;;
;; MONGO
;;

(defn mongo-connect
  "Return map with the uri, conn, db and coll needed to do a Mongo connection"
  []
  (let [uri (env :mongolab-uri)
        {:keys [conn db]} (mg/connect-via-uri uri)
        coll (env :mongo-collection)]
    {:uri uri :conn conn :db db :coll coll}))

(defn getWorkById [workid]
  (let [{:keys [uri conn db coll]} (mongo-connect)]
    (mc/find-one-as-map db coll {:id workid})))

(defn getWorks [mLimit mPage]
  (let [{:keys [uri conn db coll]} (mongo-connect)]
    (with-collection db coll
      (find {})
      (paginate :page mPage :per-page mLimit))))

(defn getAllWorksWith [f]
  (let [{:keys [uri conn db coll]} (mongo-connect)
        results  (with-collection db coll
                   (find {})
                   (fields f))]
    (map #(dissoc % :_id) results)))

(defn getAllWorks []
  (let [{:keys [uri conn db coll]} (mongo-connect)]
    (with-collection db coll
      (find {}))))

;; Test connection
(defn test-connection []
  (let [{:keys [uri conn db coll]} (mongo-connect)]
    (pprint (with-collection db coll
                             (find {})
                             (fields (vec relevant-fields))
                             (limit 3)))))

;;
;; DATOMIC
;;

(defn md5 [s]
  (let [algorithm (MessageDigest/getInstance "MD5")
        size (* 2 (.getDigestLength algorithm))
        raw (.digest algorithm (.getBytes s))
        sig (.toString (BigInteger. 1 raw) 16)
        padding (apply str (repeat (- size (count sig)) "0"))]
    (str padding sig)))

;;(def db  (d/db datomic-mgr/conn))

(defn clean-artist-name [name]
  (if (nil? name)
    (str "Unidentified Artist")
    name))

(defn artist? [name-hash]
  (let [artist-entity (datomic-q/find-artist-by-hash name-hash)]
    (if artist-entity
      artist-entity
      (d/tempid :db.part/user))))

(defn tx-for-artist [artist-item]
  (let [artist-name (clean-artist-name artist-item)
        artist-hash (md5 artist-name)
        artist-id (artist? artist-hash)
        tx-artist {:db/id       artist-id
                   :artist/name artist-name
                   :artist/hash artist-hash}]
    (if (number? artist-id)
      {:tx tx-artist :tempid {:db/id artist-id}}
      {:tx tx-artist :tempid artist-id})))

(defn processArtist [work]
  (let [name-value (:artist/name work)]
    ;; check if it's a vector
    (if (vector? name-value)
      ;; do tx for artists
      (map #(tx-for-artist % ) name-value)
      (conj () (tx-for-artist name-value)))))

(defn tx-for-artist-and-work [work]
  "Creates a list pair with tx for the artist and tx for a single work and returns a List"
  (let [pArtistWorks    (processArtist work)
        artist-ids      (into #{} (map :tempid pArtistWorks))
        tx-artist       (map :tx pArtistWorks)
        tx-work         (-> (merge work {:work/artist artist-ids})
                            (dissoc :artist/name))]
    (flatten (conj () tx-artist tx-work))))

(defn artist-transact [coll]
  "Create an tx with a [artist-tx work-tx] format"
  (-> (map tx-for-artist-and-work coll)
      flatten
      vec))

(defn processWork [work]
  "Produce a map that's been prepped for import into Datomic"
  (let [workdoc (select-keys work (vec relevant-fields))
        work-source (get-in workdoc [:_links :source :href])]
    (-> workdoc
        (update-in [:id] long)
        (clojure.set/rename-keys datomic-mongo-key-map)
        (dissoc :_links)
        (conj {source-key work-source} {:db/id (d/tempid :db.part/user)}))))


(defn process-all-works []
  (let [works (getAllWorks)]
    (->>
      (map processWork works)
      artist-transact)))

(defn write-data-out []
  (let [data-tx (process-all-works)]
    (with-open [w (clojure.java.io/writer "metmuseum-painting-data.edn")]
      (binding [*print-length* false
                *out* w]
        (pr data-tx)))))

(defn create-common-artist [name]
  (let [tx-artist (conj [] (:tx (tx-for-artist name)))
        conn (d/connect datomic-mgr/uri)]
    @(d/transact conn tx-artist)))

(defn img-url
  [metid img-data]
    (apply :image (filter #(= (:id %) metid) img-data)))

(defn img-attr [{metid :id} img-data]
  (let [tx-id {:db/id [:work/metid metid]}
        tx-img-attr {:work/image (img-url metid img-data)}
        tx-data (conj {}  tx-id tx-img-attr)]
    tx-data))

(defn import-works-to-datomic [data-type]
  (condp = data-type
    :painting (let [data-tx (read-string (slurp "metmuseum-painting-data.edn"))
                    conn (d/connect datomic-mgr/uri)]
                @(d/transact conn data-tx))
    :work-images (let [data (read-string (slurp "metmuseum-painting-images-tx.edn"))
                       conn (d/connect datomic-mgr/uri)
                       data-tx (filter #(not-nil? (:work/image %)) data)]
                   @(d/transact conn data-tx))))

(comment
  (let [mongo-work (getWorkById 35984)
        picassoHash "9716719fb465c4004381d597ac4a7107"
        wp (processWork mongo-work)]
    (def w wp))
  (create-common-artist "Unidentified Artist")
  ;; basis-t is t of most recent transaction
  (def db (d/db (d/connect datomic-mgr/uri)))
  (def basis-t (d/basis-t db))
  (def basis-tx (d/t->tx basis-t))
  ;; facts about the most recent transaction
  (d/pull db '[*] basis-tx)
  ;; log is an index of transactions
  (def log (d/log (d/connect datomic-mgr/uri)))
  ;; how many datoms in most recent transaction?
  (-> (d/tx-range log basis-tx (inc basis-tx))
    seq first :data count)

  ;; write out image data
  (let [all-work-image-data (getAllWorksWith [:id :image])]
    (with-open [w (clojure.java.io/writer "metmuseum-painting-image-data.edn")]
      (binding [*print-length* false
                *out* w]
        (pr all-work-image-data))))

  ;; write out image tx data
  (let [img-data (read-string (slurp "metmuseum-painting-image-data.edn"))
        conn (d/connect datomic-mgr/uri)]
    (with-open [w (clojure.java.io/writer "metmuseum-painting-images-tx.edn")]
      (binding [*print-length* false
                *out* w]
        (println (map #(img-attr % img-data) img-data))))
    )
  )
