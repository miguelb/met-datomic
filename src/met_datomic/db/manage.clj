(ns met-datomic.db.manage
  (:require [datomic.api :as d]
            [met-datomic.db.query :as db]
            [io.rkn.conformity :as c]
            [clojure.java.io :as io])
  (:use met-datomic.config))


(def migration :20150430-met-api-schema)

(def uri db/*db-uri*)

(def ^:dynamic conn (d/connect uri))

(defn migration-path
  [migration-name]
  (str "migrations/" (name migration-name) ".edn"))

(defn load-resource [filename]
  (-> filename
    migration-path
    io/resource
    io/reader
    slurp
    read-string))

(defn load-schema [connection schema]
  (c/ensure-conforms
    connection
      schema))

(def norms-map (load-resource migration))

(defn delete [uri]
  (d/delete-database uri))

(defn init-db [muri]
  (if (nil? muri)
    (init-db uri)
    (do
      (d/create-database muri)
      (load-schema (d/connect uri) norms-map)
      (c/ensure-conforms (d/connect uri) norms-map [:met-datomic/met-api-schema-v4])
      ;(println (str "AFTER: Has attribute? " (c/has-attribute? (d/db (d/connect uri)) :work/metid)))
      )))

