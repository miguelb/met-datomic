(ns met-datomic.core
  (:require [met-datomic.server :as server]
            met-datomic.db.manage :as db))

(defn -main
  [cmd]
  (cond
    (= cmd "server") (server/-main)))
