(ns met-datomic.config
  (:use environ.core))

(def conf
  (merge-with
    merge
    {:datomic
     {:db-uri "datomic:sql://metmusuem?jdbc:postgresql://localhost:7432/datomic?user=datomic&password=datomic"
      :test-uri "datomic:mem://metmusuem"}}))

(defn config
  [& keys]
  (get-in conf keys))

