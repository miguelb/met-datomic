(defproject met-datomic "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories {"my.datomic.com" {:url "https://my.datomic.com/repo"
                                    :creds :gpg}}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [com.cognitect/transit-clj "0.8.271"]
                 [com.fasterxml.jackson.core/jackson-annotations "2.3.2"]
                 [com.datomic/datomic-pro "0.9.5153" :exclusions [joda-time]]
                 [environ "1.0.0"]
                 [io.rkn/conformity "0.3.3" :exclusions [com.datomic/datomic-free]]
                 [org.postgresql/postgresql "9.3-1102-jdbc41"]
                 [com.novemberain/monger "2.0.0"]
                 [cursive/datomic-stubs "0.9.5153" :scope "provided"]
                 [bidi "1.18.11"]
                 [liberator "0.12.2"]
                 [compojure "1.3.4"]
                 [ring "1.3.2"]
                 [http-kit "2.1.16"]
                 [jumblerg/ring.middleware.cors "1.0.1"]
                 ]
  :plugins [[lein-environ "1.0.0"]]
  :resource-paths ["resources"])

